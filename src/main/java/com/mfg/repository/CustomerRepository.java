package com.mfg.repository;

import org.springframework.data.repository.CrudRepository;

import com.mfg.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
