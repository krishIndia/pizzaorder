package com.mfg.repository;

import org.springframework.data.repository.CrudRepository;

import com.mfg.model.CustomerOrder;

public interface OrderRepository extends CrudRepository<CustomerOrder, Long> {

}
