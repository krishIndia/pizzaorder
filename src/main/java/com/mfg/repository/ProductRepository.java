package com.mfg.repository;

import org.springframework.data.repository.CrudRepository;

import com.mfg.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
